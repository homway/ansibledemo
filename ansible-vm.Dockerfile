FROM ubuntu:20.04

ENV DEBIAN_FRONTEND=noninteractive
ENV TZ=Asia/Taipei

RUN apt-get update && \
    apt-get install openssh-server sudo lsof iputils-ping -y

RUN useradd -m -s /bin/bash ansible && \
    # echo 'root:1234' | chpasswd && \
    # echo 'ansible:1234' | chpasswd && \
    echo 'ansible ALL=(ALL) NOPASSWD: ALL' >> /etc/sudoers

COPY --chown=ansible .ssh/id_rsa.pub /home/ansible/.ssh/authorized_keys

RUN /etc/init.d/ssh restart
EXPOSE 22
CMD ["/usr/sbin/sshd","-D"]
