FROM willhallonline/ansible:2.12.2-ubuntu-20.04

ENV DEBIAN_FRONTEND=noninteractive
ENV TZ=Asia/Taipei

RUN apt-get update && \
    apt-get install openssh-server sudo lsof iputils-ping -y

WORKDIR /ansible
COPY .ssh/ /root/.ssh

RUN /etc/init.d/ssh restart
EXPOSE 22
CMD ["/usr/sbin/sshd","-D"]