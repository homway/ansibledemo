.PHONY: build

build:
	docker build -t ansible:2.12.2 -f ansible.Dockerfile .
	docker build -t ansible-vm:0.0.1 -f ansible-vm.Dockerfile .

up:
	docker-compose up -d

down:
	docker-compose down

clean:
	docker rmi ansible:2.12.2
	docker rmi ansible-vm:0.0.1

watch:
	watch -n 1 'docker ps -a --format "table{{.ID}}\t{{.Image}}\t{{.Status}}\t{{.Ports}}\t{{.Names}}"'